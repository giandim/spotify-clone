const path = require("path");

const nextConfig = {
  experimental: { appDir: true },
  reactStrictMode: true,
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
};

module.exports = nextConfig;
