import { RxCaretLeft, RxCaretRight } from "react-icons/rx";
import styles from "../../styles/Topbar.module.scss";

export default function NavigationButtons() {
  return (
    <div className={styles.iconButtons}>
      <button>
        <RxCaretLeft size={38} />
      </button>
      <button disabled>
        <RxCaretRight size={38} />
      </button>
    </div>
  );
}
