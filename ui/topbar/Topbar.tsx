import styles from '../../styles/Topbar.module.scss'
import NavigationButtons from './NavigationButtons';
import Profile from './Profile';

export default function Topbar() {
  return (
   <div className={styles.container}>
    <div><NavigationButtons /></div>
    <div>page specific actions</div>
    <div><Profile /></div>
   </div>
  );
}
