import Image from "next/image";
import { BiCaretDown } from "react-icons/bi";
import styles from "../../styles/Topbar.module.scss";

export default function Profile() {
  return (
    <button className={styles.profileButton}>
      <figure>
        <div>
          <Image
            src="https://images.unsplash.com/photo-1536164261511-3a17e671d380?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80"
            alt="Profile picture"
            fill
          />
        </div>
      </figure>
      <span>Marco Columbro</span>
      <BiCaretDown />
    </button>
  );
}
