"use client"

import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import { FaHeart, FaRegHeart } from "react-icons/fa";
import { RxCaretUp } from "react-icons/rx";
import styles from '../../styles/SongInfo.module.scss';

export default function SongInfo() {
  const [isOn, setIsOn] = useState(false);

  return (
    <div className={styles.songInfoContainer}>
      {/* ALBUM */}
      <div className={styles.albumContainer}>
        {/* EXPAND */}
        <div className={styles.expand}>
          <RxCaretUp size={38}/>
        </div>
        {/* COVER */}
        <Link href="">
          <Image
            width={55}
            height={55}
            alt="album-image"
            src="https://static-cse.canva.com/blob/978202/1600w-YmFtIRl8-qo.jpg"
          />
        </Link>
      </div>
      {/* SONG INFO */}
      <div>
        <Link href="">Song Name</Link>
        <Link href="">Artist Name</Link>
      </div>
      {/* SAVE TO LIBRARY  */}
      {isOn ? <FaHeart className={styles.heartIcon} onClick={() => setIsOn(false)}/> : <FaRegHeart onClick={() => setIsOn(true)}/>}
    </div>
  );
}
