"use client";

import styles from "../../styles/Player.module.scss";
import * as Slider from "@radix-ui/react-slider";
import { TbMicrophone2 } from 'react-icons/tb';
import { HiOutlineQueueList, HiOutlineSpeakerWave } from 'react-icons/hi2';
import { MdOutlineSpeakerGroup } from 'react-icons/md';

export default function SpecialControls() {
  return (
    <div className={styles.specialControlsContainer}>
      <button><TbMicrophone2 size={24} /></button>
      <button><HiOutlineQueueList size={24} /></button>
      <button><MdOutlineSpeakerGroup size={24} /></button>
      <button><HiOutlineSpeakerWave size={24} /></button>

      <Slider.Root
        className={styles.sliderRoot}
        max={100}
        defaultValue={[80]}
        step={1}
      >
        <Slider.Track className={styles.sliderTrack}>
          <Slider.Range className={styles.sliderRange} />
        </Slider.Track>
        <Slider.Thumb className={styles.sliderThumb} />
      </Slider.Root>
    </div>
  );
}
