import styles from '../../styles/Player.module.scss'
import PlayerControls from './PlayerControls';
import SongInfo from './SongInfo';
import SpecialControls from './SpecialControls';

export default function Player() {
  return (
   <div className={styles.container}>
    <SongInfo />
    <PlayerControls />
    <SpecialControls />
   </div>
  );
}
