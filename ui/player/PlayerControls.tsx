
'use client';
import { BiShuffle, BiPlay } from "react-icons/bi";
import { AiFillStepBackward, AiFillStepForward } from "react-icons/ai";
import { TiArrowLoop } from "react-icons/ti";
import styles from "../../styles/Player.module.scss";
import * as Slider from '@radix-ui/react-slider';

const controls = [
  { icon: <BiShuffle />, size: 24 },
  { icon: <AiFillStepBackward />, size: 24 },
  { icon: <BiPlay />, size: 48 },
  { icon: <AiFillStepForward />, size: 24 },
  { icon: <TiArrowLoop />, size: 24 },
];

export default function PlayerControls() {
  return (
    <div>
      {/* BUTTONS */}
      <div className={styles.buttonContainer}>
        <button>
          <BiShuffle size={24} />
        </button>
        <button disabled>
          <AiFillStepBackward size={24} />
        </button>
        <button className={styles.play}>
          <BiPlay size={34} />
        </button>
        <button>
          <AiFillStepForward size={24} />
        </button>
        <button>
          <TiArrowLoop size={24} />
        </button>
      </div>

      {/* BAR */}
      <div className={styles.barContainer}>
        <span>1:32</span>
        <Slider.Root className={styles.sliderRoot} max={325} defaultValue={[0]} step={1}>
          <Slider.Track className={styles.sliderTrack}>
            <Slider.Range className={styles.sliderRange}/>
          </Slider.Track>
          <Slider.Thumb className={styles.sliderThumb}/>
        </Slider.Root>
        <span>4:11</span>
      </div>
    </div>
  );
}
