import Link from "next/link";
import styles from "../styles/Navbar.module.scss";
import { MdHomeFilled, MdSearch } from 'react-icons/md';
import { VscLibrary } from "react-icons/vsc";
import { BsPlusLg, BsFillHeartFill } from "react-icons/bs";

const NavItems = [
  { text: "Home", link: "/home", icon: <MdHomeFilled size={24}/> },
  { text: "Search", link: "/search", icon: <MdSearch size={24}/> },
  { text: "Your Library", link: "/your-library", icon: <VscLibrary size={24}/> },
];

export default function Navbar() {
  return (
    <div className={styles.container}>
      {/* NAVIGATION */}
      <ul>
        {NavItems.map((item) => (
          <li>
            <Link href={item.link}> <div className={styles.iconButton}>{item.icon}</div> {item.text}</Link>
          </li>
        ))}
      </ul>

      {/* ACTIONS */}
      <ul>
        <li>
          <Link href="/"><div className={`${styles.iconButton} ${styles.whiteBackground}`}><BsPlusLg/></div> Create Playlist</Link>
        </li>
        <li>
          <Link href="/"><div className={`${styles.iconButton} ${styles.gradient}`}><BsFillHeartFill/></div> Liked Songs</Link>
        </li>
      </ul>

      {/* PLAYLIST */}
      <ul>
        <li>
          <Link href="/">JOJO openings 1-10</Link>
        </li>
        <li>
          <Link href="/">JoJo OPs & EDs</Link>
        </li>
      </ul>
    </div>
  );
}
