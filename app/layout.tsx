import { Poppins } from "@next/font/google";
import Navbar from "../ui/Navbar";
import "../styles/globals.scss";
import styles from "../styles/Home.module.scss";
import Topbar from "../ui/topbar/Topbar";
import Player from "../ui/player/Player";

const poppins = Poppins({ weight: "400" });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className={poppins.className}>
      <body>
        <div className={styles.container}>
          <nav className={styles.navbar}>
            <Navbar />
          </nav>
          <div className={styles.topbar}>
            <Topbar />
          </div>
          <div className={styles.player}>
            <Player />
          </div>
        </div>
        {children}
      </body>
    </html>
  );
}
